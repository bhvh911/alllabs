﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Ex2
{
    public class Program
    {
        static void Main(string[] args)
        {
            long firstSize;
            string firstFile = @"C:\Users\Bhvh9\Desktop\Chapter14Labs\EX2\moviequote.txt";
            FileInfo firstFileInfo = new FileInfo(firstFile);
            firstSize = firstFileInfo.Length;

            Console.WriteLine("First file size is {0}", firstSize);

            Console.WriteLine();

            long secondSize;
            string secondFile = @"C:\Users\Bhvh9\Desktop\Chapter14Labs\EX2\moviequote.txt";
            FileInfo secFileInfo = new FileInfo(secondFile);
            secondSize = secFileInfo.Length;
            Console.WriteLine("Second file size is {0}", secondSize);

            Console.WriteLine();

            Console.WriteLine("Comparing Files");
            Console.WriteLine();
            Console.WriteLine("First file size is {0}, while the second file size is {1}", firstSize, secondSize);
            
        }
    }
}
