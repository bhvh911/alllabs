﻿namespace Ex1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dirText = new System.Windows.Forms.TextBox();
            this.ouput = new System.Windows.Forms.Label();
            this.checkFile = new System.Windows.Forms.Button();
            this.display = new System.Windows.Forms.Label();
            this.fileText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chooseFile = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Test File And Directory";
            // 
            // dirText
            // 
            this.dirText.Location = new System.Drawing.Point(12, 66);
            this.dirText.Name = "dirText";
            this.dirText.Size = new System.Drawing.Size(100, 20);
            this.dirText.TabIndex = 1;
            // 
            // ouput
            // 
            this.ouput.AutoSize = true;
            this.ouput.Location = new System.Drawing.Point(284, 86);
            this.ouput.Name = "ouput";
            this.ouput.Size = new System.Drawing.Size(37, 13);
            this.ouput.TabIndex = 2;
            this.ouput.Text = "output";
            // 
            // checkFile
            // 
            this.checkFile.Location = new System.Drawing.Point(197, 49);
            this.checkFile.Name = "checkFile";
            this.checkFile.Size = new System.Drawing.Size(75, 23);
            this.checkFile.TabIndex = 3;
            this.checkFile.Text = "get directory";
            this.checkFile.UseVisualStyleBackColor = true;
            this.checkFile.Click += new System.EventHandler(this.checkFile_Click);
            // 
            // display
            // 
            this.display.AutoSize = true;
            this.display.Location = new System.Drawing.Point(9, 187);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(39, 13);
            this.display.TabIndex = 4;
            this.display.Text = "display";
            // 
            // fileText
            // 
            this.fileText.Location = new System.Drawing.Point(12, 149);
            this.fileText.Name = "fileText";
            this.fileText.Size = new System.Drawing.Size(100, 20);
            this.fileText.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(197, 147);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "get file";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chooseFile
            // 
            this.chooseFile.AutoSize = true;
            this.chooseFile.Location = new System.Drawing.Point(9, 103);
            this.chooseFile.Name = "chooseFile";
            this.chooseFile.Size = new System.Drawing.Size(136, 13);
            this.chooseFile.TabIndex = 7;
            this.chooseFile.Text = "please choose a file display";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 292);
            this.Controls.Add(this.chooseFile);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.fileText);
            this.Controls.Add(this.display);
            this.Controls.Add(this.checkFile);
            this.Controls.Add(this.ouput);
            this.Controls.Add(this.dirText);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox dirText;
        private System.Windows.Forms.Label ouput;
        private System.Windows.Forms.Button checkFile;
        private System.Windows.Forms.Label display;
        private System.Windows.Forms.TextBox fileText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label chooseFile;
    }
}

