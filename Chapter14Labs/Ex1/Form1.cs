﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ouput.Text = "";
            display.Text = "";
            chooseFile.Text = "";
        }

        private void checkFile_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string dir = dirText.Text;
                if (Directory.Exists(dir))
                {
                    sb.Append("List of files \n");
                    string[] LOFiles = Directory.GetFiles(dir);

                    for (int i = 0; i < LOFiles.Length; i++)
                    {
                        sb.Append(LOFiles[i]);
                        sb.AppendLine();
                    }
                    ouput.Text = sb.ToString();
                    
                }
                else
                {
                    ouput.Text = "Directory does not exist";
                }
            }
            catch (IOException ex)
            {
                ouput.Text = ex.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string filename = fileText.Text;
                StringBuilder sb = new StringBuilder();
                if (File.Exists(filename))
                {
                    sb.AppendLine("File exists");
                    sb.AppendLine("File was last accessed " + File.GetLastAccessTime(filename));
                    sb.AppendLine("File was created " + File.GetCreationTime(filename));
                    sb.AppendLine("File was last written to " + File.GetLastWriteTime(filename));
                    display.Text = sb.ToString();
                }
                else
                {
                    ouput.Text = "File does not exist";
                }
            }
            catch (IOException ex)
            {
                ouput.Text = ex.ToString();
            }
        }
    }
}
