﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ex3
{
    public partial class Form1 : Form
    {
        string firstFile = @"C:\Users\Bhvh9\Desktop\Chapter14Labs\EX3\hello.txt";
        string secondFile = @"C:\Users\Bhvh9\Desktop\Chapter14Labs\EX3\swoop.txt";
        string thirdFile = @"C:\Users\Bhvh9\Desktop\Chapter14Labs\EX3\bye.txt";

        public Form1()
        {
            InitializeComponent();
            checkedListBox1.Items.Add(firstFile);
            checkedListBox1.Items.Add(secondFile);
            checkedListBox1.Items.Add(thirdFile);
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string file;
            if (checkedListBox1.SelectedIndex == 0)
            {
                file = (string)checkedListBox1.SelectedItem;
                display.Text = string.Format("{0} \n file made {1}", file, File.GetCreationTime(file));
            }
            else if (checkedListBox1.SelectedIndex == 1)
            {
                file = (string)checkedListBox1.SelectedItem;
                display.Text = string.Format("{0} \n file made {1}", file, File.GetCreationTime(file));
            }
            else if (checkedListBox1.SelectedIndex == 2)
            {
                file = (string)checkedListBox1.SelectedItem;
                display.Text = string.Format("{0} \n file made {1}", file, File.GetCreationTime(file));
            }
        }
    }
}
