﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Ex4c
{
    public class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"CustomerAccount.Txt";

            Customer customer = new Customer();
            const int END = 999;
            const char DELIM = ',';

            FileStream inFile = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(inFile);
            string recordIn;
            string[] fields;

            Console.WriteLine("Enter ID Number to display information for customer");
            int idNumInput =Convert.ToInt32(Console.ReadLine());

            while (idNumInput != END)
            {
                Console.WriteLine("\n{0, -5}{1, 6}{2, 12}\n", "IDNumber", "Name", "Owed");

                inFile.Seek(0, SeekOrigin.Begin);
                recordIn = reader.ReadLine();

                while(recordIn != null)
                {
                    fields = recordIn.Split(DELIM);
                    customer.IDNumber = Convert.ToInt32(fields[0]);
                    customer.Name = fields[1];
                    customer.CurrentBalanceOwed = Convert.ToDouble(fields[2]);

                    if (idNumInput == customer.IDNumber)
                    {
                        Console.WriteLine("\n{0, -5}{1, 8}{2, 12}\n", 
                            customer.IDNumber, customer.Name, customer.CurrentBalanceOwed);
                        recordIn = reader.ReadLine();
                    }
                }
                Console.WriteLine("\nEnter ID number to find or " + END + " to quit ");
                idNumInput = Convert.ToInt32(Console.ReadLine());
            }

            reader.Close();
            inFile.Close();
        }
    }
}
