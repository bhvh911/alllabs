﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4c
{
    public class Customer
    {
        private int _idNum;
        private string _name;
        private double _currentBalance;

        

        public int IDNumber
        {
            get { return _idNum; }
            set { _idNum = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double CurrentBalanceOwed
        {
            get { return _currentBalance; }
            set { _currentBalance = value; }
        }
    }
}
