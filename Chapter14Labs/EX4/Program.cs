﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EX4
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("writer");
            Console.WriteLine();

            string filePath = @"C:\Users\Bhvh9\Desktop\Chapter14Labs\EX4\CustomAccount.txt";

            FileStream outFile = new FileStream(filePath, FileMode.Create, FileAccess.Write);


            StreamWriter writer = new StreamWriter(outFile);

            Console.WriteLine();

            Console.Write("Please enter customer ID Number: ");
            int idNumber = Convert.ToInt32(Console.ReadLine());
            Console.Write("Please enter customer name: ");
            string name = Console.ReadLine();

            Console.Write("Please enter current balance owed: ");
            double owed = Convert.ToDouble(Console.ReadLine());
            Customer customer = new Customer(idNumber, name, owed);

            writer.WriteLine(customer.IDNumber + "," + 
                customer.Name + "," + 
                customer.CurrentBalanceOwed);
            writer.Close();
            outFile.Close();

            Console.Clear();
            Console.WriteLine("file created");
        }
    }
}
