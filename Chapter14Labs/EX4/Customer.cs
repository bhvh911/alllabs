﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    public class Customer
    {
        private int _idNum;
        private string _name;
        private double _currentBalance;

        public Customer(int IdNumber, string Name, double CurrentBalance)
        {
            _name = Name;
            _idNum = IdNumber;
            _currentBalance = CurrentBalance;
        }

        public int IDNumber
        {
            get { return _idNum; }
        }

        public string Name
        {
            get { return _name; }
        }

        public double CurrentBalanceOwed
        {
            get { return _currentBalance; }
        }
    }
}
